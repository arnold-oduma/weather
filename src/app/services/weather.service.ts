import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { appInitializerFactory } from '@angular/platform-browser/src/browser/server-transition';
const groupWeatherURL = `${environment.baseURL}/group?appid=${environment.appId}&units=metric`;
const forecastrURL = `${environment.baseURL}/forecast?appid=${environment.appId}&units=metric`;

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   *get requests to retrieve data from the open weather API
   *This class is registered in the provider section of the app module
  */
  getGroupWeatherNow(locationId: any) {
    const locations = locationId.join(",");
    return this.http.get(`${groupWeatherURL}&id=${locationId}`);
  }
  getWeatherNow(locationId: any) {
    return this.http.get(`${groupWeatherURL}&id=${locationId}`);
  }
  getForecast(locationId: any) {
    return this.http.get(`${forecastrURL}&id=${locationId}`);
  }
}
