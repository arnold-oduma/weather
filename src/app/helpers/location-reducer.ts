import { Action } from '@ngrx/store';

export const stateInstance = '';
export const LOCATION = 'LOCATION'

// Reducer to manage application state and the maintain constant dat throughout the aplication
export function locReducer(state = stateInstance, action: any) {
  switch (action.type) {
    case LOCATION:
      state = action.payload
      return state;
    default:
      return state;
  }
}