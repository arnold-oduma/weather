import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { WeatherService } from 'src/app/services/weather.service';
import { Observable } from 'rxjs';
import * as CanvasJS from '../../../assets/scripts/canvasjs.min.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  location: string;
  loc$: Observable<string>;
  weatherNow: any;
  groupWeatherNow: any;
  forecast: any = {}; 

  loading = false;

  groupLocations = [
    2761369,
    6458923,
    2643743,
    3054643,
    2968815
  ]

  constructor(
    private store: Store<any>,
    private weatherService: WeatherService
  ) {

    /**
     * This method subscribes to the weather api data from the api call
     */
    this.loc$ = store.pipe(select('location'));
    this.loc$.subscribe(
      location => {
        this.location = location;
        this.getMultipleLocations(this.groupLocations)
      }
    )
  }

  ngOnInit() {
  }
  /**
   *
   * @param groupLocations
   * This method uses the group location parameter to query the api for data for multiple predefined cities
   */
  getMultipleLocations(groupLocations: any) {
    this.weatherService.getGroupWeatherNow(this.groupLocations)
      .subscribe(res => {
        this.groupWeatherNow = res;
        this.getForecast(this.groupWeatherNow.list[0].id);
      }, err => {
        alert(err);
        console.log(err);
        alert('An error occured while getting the group weather')
      }
      )
  }

  /**
   *
   * @param cityId
   * This method gets the api forecast for a given city id and stores ir=t in the forecast variable
   */
  getForecast(cityId: number) {
    this.loading = true;
    this.forecast = [];
    this.weatherService.getForecast(cityId)
      .subscribe(res => {
        this.forecast = res;
        this.chart(this.forecast);

      }, err => {
        alert('An error occured while getting the weather')
      },() => this.loading = false
      )
  }


  /**
   *
   * @param data
   * This method loads an array of forecast dats to the chart
   */
  chart(data: any) {
    let dataPoints = [];
    let dataPoints2 = [];
    data.list.forEach(dayCast => {
      let date = (dayCast.dt*1000).toLocaleString;
      dataPoints.push({ y: dayCast.main.temp, name: dayCast.weather[0].main});
    });

    /**
     * Creating a canvasjs chart to populate a line graph with forecast temperatures at a three hour interval
     */
    let chart = new CanvasJS.Chart("chartContainer", {
      theme: "light2",
      zoomEnabled: true,
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: data.city.name + " Weather Pattern"
      },
      axisX: {
        title: "Day"
      },
      axisY: {
        title: "Temp Celsius"
      },
      data: [{
        type: "spline",
        lineColor: "pink",
        showInLegend: true,
        toolTipContent: "{y}<b>&#8451; {name}</b>: )",
        dataPoints: dataPoints
      }
    ]
    });
    chart.render();
  }



}
