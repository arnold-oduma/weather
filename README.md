# Weather

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

This is an angular 7 weather application that gets weather data from the Open Weather API.

The application displays the data of five major cities and upon clicking on a city you get to see the forecast for the following 5 days. This forecast is in 3 hour intervals.

This application also displays a line graph which shows the trend of the forecast pattern.

### Architecture

I used the model view control achitecture to create this application.

The application comunicates with the api through a service, this is to ensure that the api calls code can be shared by calling the service in any component iin the application.

I used ngrx store to act as a central data storage and maintain the state of the application data throughout ever component. ie centralized state management

Observables with came about with reactive programing are used in this application to get data from the api calls and in turn populate it on the views.

I have hosted the application on firebase on the link bellow

## Link:  [https://weather-254.firebaseapp.com](https://weather-254.firebaseapp.com).

## DEMO

  [![alt text](readmeImgs/sh2.png)](https://weather-254.firebaseapp.com)
  ![alt text](readmeImgs/sh1.png){:height="300px"}
  ![alt text](readmeImgs/sh5.png)
  ![alt text](readmeImgs/sh3.png)
  ![alt text](readmeImgs/sh4.png)


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


# Author

Made with love by:
## Arnold Oduma

### Portfolio - [Arnold's Portfolio](https://portfolio-937b2.firebaseapp.com/portfolio)